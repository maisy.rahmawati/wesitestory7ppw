$(document).ready(function() {
	$("#ganti_tema1").click(function() {
		$("body").css({"background-color":"#B0C4DE"});
		$(".navbar").css({"background-color":"black"});
		$(".content_page").css({"background-color":"#778899"});
		$(".description p").css({"color":"white"});
		$(".menu_accordion").css({"background-color":"#DCDCDC"});
		$(".tampilan").css({"background-color":"#778899"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	$("#ganti_tema2").click(function() {
		$("body").css({"background-color":"#FA8072"});
		$(".navbar").css({"background-color":"#8B4513"});
		$(".content_page").css({"background-color":"#FFDAB9"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"#FFEBCD"});
		$(".tampilan").css({"background-color":"#8B4513"});
		$(".tampilan p").css({"color":"black"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	$("#ganti_tema3").click(function() {
		$("body").css({"background-color":"#800000"});
		$(".navbar").css({"background-color":"black"});
		$(".content_page").css({"background-color":"rgba(255,255,255,0.8)"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"rgba(255,255,255,0.8)"});
		$(".tampilan").css({"background-color":"rgb(139, 5, 0, 0.7)"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
		
	});
	$("#ganti_tema4").click(function() {
		$("body").css({"background-color":"rgb(173,255,48,0.75)"});
		$(".navbar").css({"background-color":"#008000"});
		$(".content_page").css({"background-color":"#BDB76B"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"#F0E68C"});
		$(".tampilan").css({"background-color":"rgb(128, 128, 1, 0.9)"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	
	$(".menu_accordion .tampilan")
			//mengambil elemen selanjutnya/elemen yang mengikuti yaitu class isi_tampilan
            .next(".isi_tampilan")
			/*
			*menyeleksi selector yang sudah diberikan id/class yang ingin diseleksi
			*dengan tidak dari element yang pertama(first)
			*/
            .filter(":not(:first)")
			//menyembunyikan element HTML
            .hide();

    $(".tampilan").click(function(){
		/*
		*&(this) mengacu pada class tampilan yang diclick
		*.next("div:first") akan mengambil element tag <div> pertama yang mengikutinya
		*.is(":visible") maksudnya tag <div> akan diseleksi oleh selector :visible jika dia memiliki ruang
		*/
        var selfClick = $(this).next("div:first").is(":visible");
		//jika boolean dari selfClick bernilai true maka akan mereturn tampilan dari tag <div>
        if(selfClick) {
            return;
        }
        $(this)
			/*
			*$(this).parent() mengakses/mengembalikan set yang berisi dokumen dari parentnya yaitu class tampilan
			*yang mana dari class parentnya(tampilan), isi_tampilan yang diperoleh dari $(this).next("div:first") akan di hide(disembunyikan)
			*/
            .parent()
			//mencari dan menyeleksi isi_tampilan dengan selector :visible
            .find("> div:visible")
			//menyembunyikan element di dalam tag<div> (element dengan class isi_tampilan) dengan effect slide
            .slideToggle();

        $(this)
			//.next("div:first") akan mengambil element tag <div> pertama yang mengikutinya
            .next("div:first")
			//menampilkan element di dalam tag<div> (element dengan class isi_tampilan)dengan effect slide
            .slideToggle();
    });
});