from django.test import TestCase,Client
from django .http import HttpRequest
from .views import homepage
from django.urls import resolve

from selenium import webdriver
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek alamat url homepage telah ada
    def test_url_address_homepage_is_exist(self):
        response = Client().get('/homepage/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url homepage tidak ada
    def test_url_address_homepage_is_not_exist(self):
        response = Client().get('/homepage')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi homepage telah memiliki isi
    def test_add_form_is_written(self):
        self.assertIsNotNone(homepage)

    #Test untuk cek template html telah digunakan pada halaman homepage
    def test_template_html_add_form_is_exist(self):
        response = Client().get('/homepage/')
        self.assertTemplateUsed(response, 'homepage.html')

    #Test untuk cek apakah string accordion telah ada di halaman homepage
    def test_content_accordion_in_html_homepage_is_exist(self):
        response = Client().get('/homepage/')
        target = resolve('/homepage/')
        self.assertContains(response,'What is your busy schedule right now?')
        self.assertContains(response,'What is your favorite hobby?')
        self.assertContains(response,'What was your dream when you graduated?')
        self.assertContains(response,'What dreams and hopes have you achieved?')
        self.assertContains(response,'What difficulties are you facing right now?')
        self.assertTrue(target.func, homepage)

class FunctionalTest(unittest.TestCase):
    #Melakukan setUp untuk starting web browser, yaitu dengan menggunakan google chrome
    def setUp(self):
        self.browser = webdriver.Chrome()

    #Melakukan tearDown dengan mengeluarkan web browser
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    #Melakukan test functional ke website yang telah dibuat dan dideployment
    def test_functional(self):
        self.browser.get('https://maisy-websitestory7ppw.herokuapp.com/homepage/')
        time.sleep(5)
        self.assertIn('Javascript', self.browser.title)
        self.assertIn('What is your busy schedule right now?', self.browser.page_source)
        self.assertIn('What is your favorite hobby?', self.browser.page_source)
        self.assertIn('What was your dream when you graduated?', self.browser.page_source)
        self.assertIn('What dreams and hopes have you achieved?', self.browser.page_source)
        self.assertIn('What difficulties are you facing right now?', self.browser.page_source)

if __name__ == '__main__':
   unittest.main(warnings='ignore')
